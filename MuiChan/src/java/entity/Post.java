/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package entity;

import java.util.Date;

/**
 *
 * @author admin
 */
public class Post {
    private int PostId;
    private int userId;
    private String content;
    private String image;
    private Date postDate;
    private boolean status;

    public Post() {
    }

    public Post(int PostId, int userId, String content, String image, Date postDate, boolean status) {
        this.PostId = PostId;
        this.userId = userId;
        this.content = content;
        this.image = image;
        this.postDate = postDate;
        this.status = status;
    }

    public Post(int userId, String content, String image, Date postDate, boolean status) {
        this.userId = userId;
        this.content = content;
        this.image = image;
        this.postDate = postDate;
        this.status = status;
    }

    public Post(String content, String image, Date postDate, boolean status) {
        this.content = content;
        this.image = image;
        this.postDate = postDate;
        this.status = status;
    }
    
    
}
